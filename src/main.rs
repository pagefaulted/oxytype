use std::error::Error;

use oxytype::application;

fn main() -> Result<(), Box<dyn Error>> {
    application::application_loop()
}


