use std::io::Stdout;

use crossterm::event::KeyCode;
use crossterm::event::KeyModifiers;
use tui::Frame;

use tui::backend::CrosstermBackend;
use tui::layout::Alignment;
use tui::layout::Constraint;
use tui::layout::Direction;
use tui::layout::Layout;
use tui::style::Color;
use tui::style::Style;
use tui::widgets::Paragraph;

use super::ApplicationPage;
use super::State;
use super::done_page::Done;
use super::typing_test_page::TypingTest;

pub struct Summary {
    pub state: State,
    test_time_s: u16,
    typed_characters_count: u16,
    errors_count:u16
}

impl Summary {
    pub fn new(test_time_s: u16, typed_characters_count: u16, errors_count:u16) -> Self {
        Summary { state: State::SummaryState, test_time_s, typed_characters_count, errors_count }
    }
}

impl ApplicationPage for Summary {
    fn handle_key_for_tick(
        &mut self,
        key_code: crossterm::event::KeyCode,
        modifiers: crossterm::event::KeyModifiers
    ) -> Option<Box<dyn ApplicationPage>> {

        match (key_code, modifiers) {
            (KeyCode::Char('c'), KeyModifiers::CONTROL) => Some(Box::new(Done::new())),
            (KeyCode::Char('r'), KeyModifiers::CONTROL) => Some(Box::new(TypingTest::new_from_binary())),
            _ => None
        }
    }

    fn draw(&self, frame: &mut Frame<CrosstermBackend<Stdout>>) {
        let size = frame.size();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(2)
            .constraints(
                [
                    Constraint::Length(1),
                ].as_ref(),
            )
            .split(size);

        // Divide by the average english word length (5) and normalize to 60 seconds.
        // Errors do not count against typed chars.
        // TODO: make normalization a function of the test time.
        let wpm = ((self.typed_characters_count - self.errors_count) / 5) * 4;
        let summary_text = format!("Test time: {}s\nWPM: {}\nErrors count: {}", self.test_time_s, wpm, self.errors_count);

        // Header
        let header_section = Paragraph::new(format!("Summary\n\n{}", summary_text))
            .style(Style::default().fg(Color::Magenta))
            .alignment(Alignment::Center);


        frame.render_widget(header_section, chunks[0]);
    }

    fn state(&self) -> &State {
        &self.state
    }
}
