use std::time::Duration;

#[derive(Copy,Clone)]
pub struct Config {
    pub tick_rate: Duration,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            tick_rate: Duration::from_millis(200),
        }
    }
}
