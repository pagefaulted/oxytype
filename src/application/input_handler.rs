use std::{sync::mpsc::{Receiver, self}, thread::{JoinHandle, self}, time::{Duration, Instant}};

use crossterm::event::{Event as CEvent, KeyEvent, self};

use InputHandlerEvent::Input;

use super::config::Config;

pub fn setup_input_channel(config: &Config, owner_rx: Receiver<InputThreadCommand>)
-> (Receiver<InputHandlerEvent<KeyEvent>>, JoinHandle<()>) {

    let (tx, rx) = mpsc::channel();
    let tick_rate = config.tick_rate;

    // Fork thread for channel input loop.
    let input_thread = thread::spawn(move || {
        let mut last_tick = Instant::now();

        loop {
            if let Ok(InputThreadCommand::Terminate) = owner_rx.try_recv() {
                break;
            }

            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout).expect("Poll should not return Err") {
                if let CEvent::Key(key) = event::read().expect("Read should not fail") {
                    tx.send(Input(key)).expect("Send should not fail");
                }
            }

            // Send Tick if tick rate is elapsed.
            if last_tick.elapsed() > tick_rate {
                if let Ok(_) = tx.send(InputHandlerEvent::Tick) {
                    last_tick = Instant::now();
                }
            }
        }
    });

    (rx, input_thread)
}

pub enum InputHandlerEvent<I> {
    Input(I),
    Tick,
}

pub enum InputThreadCommand {
    Terminate
}
