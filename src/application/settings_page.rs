use std::io::Stdout;

use tui::backend::CrosstermBackend;
use tui::Frame;

use super::ApplicationPage;
use super::State;

pub(crate) struct Settings {
    pub(crate) state: State
}

impl Settings {
    #[allow(dead_code)]
    pub(crate) fn new() -> Self {
        Settings { state: State::SettingsState }
    }
}

impl ApplicationPage for Settings {
    fn handle_key_for_tick(
        &mut self,
        _key_code: crossterm::event::KeyCode,
        _modifiers: crossterm::event::KeyModifiers
    ) -> Option<Box<dyn ApplicationPage>> {
        None
    }

    fn draw(&self, _frame: &mut Frame<CrosstermBackend<Stdout>>) {
        panic!("Unsupported operation for this state Init!");
    }

    fn state(&self) -> &State {
        &self.state
    }
}
