use std::io::Stdout;

use tui::backend::CrosstermBackend;
use tui::Frame;

use super::ApplicationPage;
use super::State;

pub struct Done {
    pub state: State
}

impl Done {
    pub fn new() -> Self {
        Done { state: State::DoneState }
    }
}

impl ApplicationPage for Done {
    fn handle_key_for_tick(
        &mut self,
        _key_code: crossterm::event::KeyCode,
        _modifiers: crossterm::event::KeyModifiers
    ) -> Option<Box<dyn ApplicationPage>> {
        None
    }

    fn draw(&self, _frame: &mut Frame<CrosstermBackend<Stdout>>) {
        panic!("Unsupported operation for this state Init!");
    }

    fn state(&self) -> &State {
        &self.state
    }
}
