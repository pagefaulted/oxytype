use std::io::Stdout;

use tui::layout::Alignment;
use tui::style::Color;
use tui::style::Style;
use tui::text::Span;
use tui::text::Spans;
use tui::widgets::Block;
use tui::widgets::Borders;
use tui::widgets::Paragraph;
use tui::layout::Constraint;
use tui::layout::Direction;
use tui::layout::Layout;
use tui::backend::CrosstermBackend;
use tui::Frame;

use crossterm::event::KeyModifiers;
use crossterm::event::KeyCode;

use super::ApplicationPage;
use super::State;
use super::done_page::Done;
use super::typing_test_page::TypingTest;

pub struct Main {
    pub state: State
}

impl Main {
    pub fn new() -> Self {
        Main { state: State::MainState }
    }
}

impl ApplicationPage for Main {

    fn handle_key_for_tick(&mut self, key_code: KeyCode, modifiers: KeyModifiers) -> Option<Box<dyn ApplicationPage>> {
        match (key_code, modifiers) {
            (KeyCode::Char('c'), KeyModifiers::CONTROL) => Some(Box::new(Done::new())),
            (KeyCode::Char('r'), _) => Some(Box::new(TypingTest::new_from_binary())),
            _ => None
        }
    }

    fn draw(&self, frame: &mut Frame<CrosstermBackend<Stdout>>) {
        let size = frame.size();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(2)
            .constraints(
                [
                    Constraint::Length(1),
                    Constraint::Percentage(30),
                    Constraint::Percentage(50),
                ].as_ref(),
            )
            .split(size);

        // Header
        let header_section = Paragraph::new("OxyType")
            .style(Style::default().fg(Color::Magenta))
            .alignment(Alignment::Center);

        let bullets = Spans::from(vec![
            Span::raw("• "),
            Span::styled("Press Ctrl+r to start or re-start a Typing Test.", Style::default().fg(Color::Green))
        ]);

        let bullets_paragraph = Paragraph::new(bullets).block(Block::default().borders(Borders::NONE));

        let filler = Paragraph::new("");

        frame.render_widget(header_section, chunks[0]);
        frame.render_widget(filler, chunks[1]);
        frame.render_widget(bullets_paragraph, chunks[2]);
    }

    fn state(&self) -> &State {
        &self.state
    }
}
