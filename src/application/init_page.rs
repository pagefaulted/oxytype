use std::io::Stdout;

use tui::backend::CrosstermBackend;
use tui::Frame;

use super::ApplicationPage;
use super::State;

pub struct Init {
    pub state: State
}

impl Init {
    pub fn new() -> Self {
        Init { state: State::InitState }
    }
}

impl ApplicationPage for Init {
    fn handle_key_for_tick(
        &mut self,
        _key_code: crossterm::event::KeyCode,
        _modifiers: crossterm::event::KeyModifiers
    ) -> Option<Box<dyn ApplicationPage>> {
        None
    }

    fn draw(&self, _frame: &mut Frame<CrosstermBackend<Stdout>>) {
        panic!("Unsupported operation for this state Init!");
    }

    fn state(&self) -> &State {
        &self.state
    }
}