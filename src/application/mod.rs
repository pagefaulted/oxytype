use std::io::Stdout;
use std::sync::mpsc::{Receiver, self};
use std::io;

use crossterm::event::{KeyEvent, KeyCode, KeyModifiers};
use crossterm::terminal::{disable_raw_mode, enable_raw_mode};

use tui::backend::CrosstermBackend;
use tui::{Frame, Terminal};

use config::Config;
use init_page::Init;
use input_handler::setup_input_channel;
use input_handler::InputHandlerEvent;
use input_handler::InputHandlerEvent::Input;
use input_handler::InputThreadCommand;

mod config;
mod done_page;
mod input_handler;
mod init_page;
mod main_page;
mod settings_page;
mod summary_page;
mod typing_test_page;

struct Session {
    application_state_machine: Box<ApplicationStateMachine>,
    config: Config
}

impl Default for Session {
    fn default() -> Self {
        Self { application_state_machine: ApplicationStateMachine::new(), config: Config::default() }
    }
}

pub enum State {
    InitState,
    MainState,
    SettingsState,
    TypingTestState,
    SummaryState,
    DoneState,
}

trait ApplicationPage {

    /// Implementors can decide how to handle the key/modifiers pair for the current tick.
    ///
    /// It either returns None, which keeps the application on the current page, or an `ApplicationPage` instance,
    /// which will change the application's current page.
    fn handle_key_for_tick(&mut self, key_code: KeyCode, modifiers: KeyModifiers) -> Option<Box<dyn ApplicationPage>>;

    fn handle_tick(&mut self) -> Option<Box<dyn ApplicationPage>> {
        None
    }

    fn handle_input(&mut self, rec: &Receiver<InputHandlerEvent<KeyEvent>>) -> Option<Box<dyn ApplicationPage>> {
        match rec.recv().expect("recv Panic!") {
            Input(KeyEvent { code, modifiers, kind: _kind, state: _state }) => {
                self.handle_key_for_tick(code, modifiers)
            }
            _ => self.handle_tick()
        }
    }

    // TODO: can the generic be a superclass of Crossterm?
    fn draw(&self, frame: &mut Frame<CrosstermBackend<Stdout>>);

    fn state(&self) -> &State;
}

struct ApplicationStateMachine {
    current_state: Box<dyn ApplicationPage>
}

impl ApplicationStateMachine {

    fn tick(&mut self, rec: &Receiver<InputHandlerEvent<KeyEvent>>) {
        match self.current_state.handle_input(rec) {
            None => {}
            Some(new_state) => {
                match (self.current_state.state(), new_state.state()) {
                    (State::InitState, State::MainState) => {},
                    (State::MainState, State::SettingsState) => {},
                    (State::MainState, State::TypingTestState) => {},
                    (State::SettingsState, State::MainState) => {},
                    (State::TypingTestState, State::MainState) => {},
                    (State::TypingTestState, State::SummaryState) => {},
                    (State::TypingTestState, State::TypingTestState) => {},
                    (State::SummaryState, State::MainState) => {},
                    (State::SummaryState, State::TypingTestState) => {},
                    (_, State::DoneState) => {}
                    _ => panic!("Invalid ApplicationState transition!")
                }
                self.current_state = new_state;
            }
        }
    }

    pub fn new() -> Box<ApplicationStateMachine> {
        Box::new(ApplicationStateMachine { current_state: Box::new(Init::new()) })
    }
}

pub fn application_loop() -> Result<(), Box<dyn std::error::Error>> {
    let mut session = Session::default();

    enable_raw_mode()?;

    let backend = CrosstermBackend::new(io::stdout());
    let mut term = Terminal::new(backend)?;
    term.clear()?;

    // Fork input thread.
    let (input_thread_tx, input_thread_rx) = mpsc::channel();
    // TODO: consider attaching the receiver to the session struct and dealing with the associated lifetime headache.
    let (rec, input_thread_handle) = setup_input_channel(&session.config, input_thread_rx);

    session.application_state_machine.current_state = Box::new(main_page::Main::new());

    loop {
        session.application_state_machine.tick(&rec);
        match session.application_state_machine.current_state.state() {
            State::DoneState => break,
            _ => {
                term.draw(|frame| {
                    session.application_state_machine.current_state.draw(frame);
                })?;
            }
        }
    }

    // Terminate input thread.
    input_thread_tx.send(InputThreadCommand::Terminate)?;
    input_thread_handle.join().expect("Input thread failed to join.");

    term.clear()?;
    disable_raw_mode()?;

    Ok(())
}
