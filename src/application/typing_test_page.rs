use core::panic;
use std::collections::HashSet;
use std::fs::File;
use std::io::{Stdout, BufReader, BufRead, Read};
use std::time::Instant;

use crossterm::event::{KeyCode, KeyModifiers};

use rand::distributions::{Uniform, Distribution};
use rand::{Rng, thread_rng};

use rand::seq::SliceRandom;
use tui::Frame;
use tui::backend::CrosstermBackend;
use tui::layout::{Alignment, Constraint, Layout, Direction};
use tui::style::{Color, Modifier, Style};
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders, BorderType, Paragraph, Wrap};

use crate::application::typing_test_page::KeyEventResult::{Quit, KeyHandled, Restart, NoKey, BackspaceHandled};
use crate::application::State;

use super::ApplicationPage;
use super::done_page::Done;
use super::summary_page::Summary;

static TEST_TEXT: &str = "do not open consider line text through computer female accent each home \
use not so much but a never table light cancel fantastic juno great luna moon jupiter planets \
accrue other terminal not only will have there within weather sky yard grass roof house finished \
do not open consider line text through computer female accent each home \
use not so much but a never table light cancel fantastic juno great luna moon jupiter planets \
accrue other terminal not only will have there within weather sky yard grass roof house finished";

#[allow(dead_code)]
static FILE_PATH: &str = "./resources/english";

#[derive(Debug)]
enum KeyEventResult {
    Restart,
    Quit,
    KeyHandled,
    BackspaceHandled,
    NoKey,
}

pub struct TextHandler {
    text: String,
    input_text: String,
}

impl TextHandler {

    pub fn new(text: String) -> TextHandler {
        TextHandler { text, input_text: "".to_string() }
    }

    fn handle_key(&mut self, key_code: KeyCode, modifiers: KeyModifiers) -> KeyEventResult {
        match (key_code, modifiers) {
            (KeyCode::Char('c'), KeyModifiers::CONTROL) => Quit,
            (KeyCode::Char('r'), KeyModifiers::CONTROL) => Restart,
            (KeyCode::Char(c), _) => {
                self.input_text.push(c);
                KeyHandled
            }
            (KeyCode::Backspace, _) => {
                self.input_text.pop();
                BackspaceHandled
            }
            _ => NoKey
        }
    }

    pub fn create_paragraph(&self) -> Paragraph {
        // Identify matching characters.
        let transitions: Vec<bool> = self.input_text
            .chars()
            .zip(self.text.chars())
            .map(|(input_char, text_char)| { input_char == text_char })
            .collect();


        let mut spans: Vec<Span> = vec![];
        let mut idx = 0;
        // Create a span for each block of matching/not-matching characters.
        while idx < transitions.len() {
            let start_of_block_idx = idx;

            let current_value = transitions[idx];

            while idx + 1 < transitions.len() && transitions[idx + 1] == current_value {
                // Move the index forward until a transition is found.
                idx += 1;
            }

            // Create a span for the current block of characters.
            spans.push(self.create_span(start_of_block_idx, idx + 1, current_value));

            idx += 1;
        }

        spans.push(Span::raw(&self.text[transitions.len()..]));

        Paragraph::new(Spans::from(spans))
            .wrap(Wrap { trim: false }).block(Block::default()
            .border_type(BorderType::Rounded)
            .borders(Borders::NONE))
    }

    fn create_span(&self, start_idx: usize, end_idx: usize, is_match: bool) -> Span {
        match is_match {
            true => Span::styled(&self.text[start_idx..end_idx], Style::default().bg(Color::DarkGray).add_modifier(Modifier::UNDERLINED)),
            false => Span::styled(&self.text[start_idx..end_idx], Style::default().bg(Color::Red).add_modifier(Modifier::BOLD)),
        }
    }
}

pub struct TypingTest {
    state: State,
    text_handler: TextHandler,
    // Initialized on the first character input.
    elapsed_time: Option<Instant>,
}

impl TypingTest {
    #[allow(dead_code)]
    pub fn new_from_file() -> Self {
        let text = FileTextRetriever::new(FILE_PATH.to_string()).get_text();
        TypingTest {
            state: State::TypingTestState,
            text_handler: TextHandler::new(text),
            elapsed_time: None,
        }
    }

    pub fn new_from_binary() -> Self {
        let text: String = BinaryTextRetriever::new().get_text();
        TypingTest {
            state: State::TypingTestState,
            text_handler: TextHandler::new(text),
            elapsed_time: None,
        }
    }

    fn create_summary(&self) -> Option<Box<dyn ApplicationPage>> {
        let elapsed_time: u16 = match self.elapsed_time {
            Some(instant) => instant.elapsed().as_secs() as u16,
            _ => 0
        };

        let errors_count = self.text_handler.input_text
            .chars()
            .zip(self.text_handler.text.chars())
            .filter(|(x, y)| x != y)
            .count() as u16;

        // TODO: make elapsed time the configured time rather than the actual time.
        let non_spaces_chars = self.text_handler.input_text.chars().filter(|&c| c != ' ').count() as u16;
        Some(Box::new(Summary::new(elapsed_time, non_spaces_chars, errors_count)))
    }

    fn init_start_instant(&mut self) {
        match self.elapsed_time {
            None => self.elapsed_time = Some(Instant::now()),
            _ => {}
        }
    }

    fn is_time_elapsed(&self) -> bool {
        if let Some(elapsed_time) = self.elapsed_time {
            // TODO: make this time configurable via Settings.
            elapsed_time.elapsed().as_secs() > 15
        } else {
            false
        }
    }


}

impl ApplicationPage for TypingTest {
    fn handle_key_for_tick(&mut self, key_code: KeyCode, modifiers: KeyModifiers) -> Option<Box<dyn ApplicationPage>> {
        if self.is_time_elapsed() {
            self.create_summary()
        } else {
            match self.text_handler.handle_key(key_code, modifiers) {
                KeyHandled => {
                    self.init_start_instant();
                    None
                },
                Quit => Some(Box::new(Done::new())),
                Restart => Some(Box::new(TypingTest::new_from_binary())),
                _ => None
            }
        }
    }

    fn handle_tick(&mut self) -> Option<Box<dyn ApplicationPage>> {
        if self.is_time_elapsed() {
                self.create_summary()
        } else {
            None
        }
    }

    fn draw(&self, frame: &mut Frame<CrosstermBackend<Stdout>>) {
        let size = frame.size();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(2)
            .constraints(
                [
                    Constraint::Percentage(10),
                    Constraint::Percentage(40),
                    Constraint::Percentage(50)
                ].as_ref(),
            )
            .split(size);

        let text_area = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(15), Constraint::Percentage(70), Constraint::Percentage(15)].as_ref())
        .split(chunks[2]);

        let filler_left = Block::default().borders(Borders::NONE);
        let filler_right = Block::default().borders(Borders::NONE);

        // Header
        let header_section = Paragraph::new("OxyType Typing Test")
            .style(Style::default().fg(Color::Magenta))
            .alignment(Alignment::Center);

        let filler = Block::default().borders(Borders::NONE);

        let text_section = self.text_handler.create_paragraph();
        frame.render_widget(header_section, chunks[0]);
        frame.render_widget(filler, chunks[1]);
        frame.render_widget(filler_left, text_area[0]);
        frame.render_widget(text_section, text_area[1]);
        frame.render_widget(filler_right, text_area[2]);
    }

    fn state(&self) -> &State {
        &self.state
    }
}

trait TextRetriever {
    fn get_text(&self) -> String;
}

struct FileTextRetriever {
    file_path: String,
    words_count: u16
}

impl FileTextRetriever {
    fn new(file_path: String) -> FileTextRetriever {
        if let Ok(f) = File::open(&file_path) {
            let words_count = BufReader::new(f).lines().count() as u16;
            FileTextRetriever { file_path, words_count }
        } else {
            panic!("Could not open file")
        }
    }
}

impl TextRetriever for FileTextRetriever {
    fn get_text(&self) -> String {
        let f = File::open(&self.file_path).unwrap();
        let mut lines = BufReader::new(f).lines();
        let mut random_word_ids: HashSet<u16> = HashSet::with_capacity(40);

        while random_word_ids.len() < 40 {
            let random_num = rand::thread_rng().gen_range(0..self.words_count);
            random_word_ids.insert(random_num);
        }

        let mut random_word_ids: Vec<u16> = random_word_ids.into_iter().collect();
        // Must sort, because the operation used to get the nth line will consume the iterator.
        random_word_ids.sort();

        let mut offset = 0;
        for idx in random_word_ids.iter_mut() {
            let current_idx = *idx;
            *idx = *idx - offset;
            offset = current_idx + 1;
        }

        let mut words: Vec<String> = Vec::with_capacity(random_word_ids.len());
        for nth in random_word_ids {
            words.push(lines.nth(nth as usize).unwrap().unwrap());
        }

        words.shuffle(&mut thread_rng());

        words.join(" ")
    }
}

struct StaticTextRetriever;

impl TextRetriever for StaticTextRetriever {
    fn get_text(&self) -> String {
        TEST_TEXT.to_string()
    }
}

struct BinaryTextRetriever {
    dictionary: &'static [u8],
}

impl BinaryTextRetriever {
    fn new() -> BinaryTextRetriever {
        BinaryTextRetriever { dictionary: include_bytes!("../../resources/english") }
    }

    fn get_word_at(&self, position: u16) -> String {
        let mut curr_word: u16 = 0;
        let mut word_starting_idx: usize = 0;
        let mut curr_idx: usize = 0;

        while curr_idx < self.dictionary.len() {
            // Find line terminator.
            if self.dictionary[curr_idx] != b'\n' {
                curr_idx += 1;
                continue;
            }

            if position == curr_word {
                // Word found.
                // TODO: consider avoiding the copy and working with slices instead.
                return std::str::from_utf8(&self.dictionary[word_starting_idx..curr_idx])
                .expect("The dictionary contained an invalid word")
                .to_string()
            } else {
                // Move to the next word.
                curr_word += 1;
                curr_idx += 1;
                word_starting_idx = curr_idx;
            }
        }
        panic!("The dictionary does not contain a word at the requested position {}!", position);
    }
}

impl TextRetriever for BinaryTextRetriever {
    fn get_text(&self) -> String {
        // Scan to get words count.
        let words_count = self.dictionary.bytes().filter(|rb| {
            *rb.as_ref().unwrap() == b'\n'
        })
        .count() as u16;

        let between = Uniform::from(0..words_count);
        let mut rng = rand::thread_rng();

        // TODO: the words retrieved here should be a function of the test time.
        (0..40)
        .map(|_| between.sample(&mut rng))
        .map(|pos| self.get_word_at(pos))
        .collect::<Vec<String>>()
        .join(" ")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_word_at() {
        let binary_text_retriever = BinaryTextRetriever::new();
        assert_eq!(binary_text_retriever.get_word_at(0), "a");
        assert_eq!(binary_text_retriever.get_word_at(999), "yourself");
    }

    #[test]
    #[should_panic(expected = "The dictionary does not contain a word at the requested position")]
    fn test_get_word_out_of_bound() {
        let binary_text_retriever = BinaryTextRetriever::new();
        binary_text_retriever.get_word_at(1000);
    }
}